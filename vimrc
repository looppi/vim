set nocompatible

" Pathogen
call pathogen#infect()
call pathogen#helptags()
 
"set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
" Statusline with Syntastic
set statusline=%<%f%y\ %#warningmsg#%{SyntasticStatuslineFlag()}%*\ %h%m%r%=%-14.(%l/%L,%c%V%)\ %P
filetype plugin indent on

call plug#begin('~/.vim/plugged')
Plug '~/.fzf'
Plug 'junegunn/fzf.vim'
Plug 'editorconfig/editorconfig-vim'
call plug#end()

syntax on
filetype on
set number
set mouse=a
set mousehide

set hlsearch
set showmatch
set incsearch
set ignorecase
set autoindent
set history=1000
set cursorline
set completeopt=menu
if has("unnamedplus")
  set clipboard=unnamedplus
elseif has("clipboard")
  set clipboard=unnamed
endif

set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4

autocmd BufNewFile,BufRead *.styl set filetype=stylus
autocmd Filetype javascript setlocal ts=2 sw=2 expandtab
autocmd Filetype ruby setlocal ts=2 sw=2 expandtab
autocmd Filetype stylus setlocal ts=4 sw=4 sts=0 noexpandtab
autocmd Filetype jade setlocal ts=4 sw=4 sts=0 noexpandtab
autocmd Filetype yaml setlocal syntax=off ts=2 sw=2 expandtab
autocmd FileType tf setlocal commentstring=#\ %s


" Folding
set foldlevel=9999 " initially open all folds
command FoldAll set foldlevel=0
command FoldOne set foldlevel=1

" Nerdtree
" autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
let NERDTreeShowBookmarks=1
let NERDTreeChDirMode=0
let NERDTreeQuitOnOpen=0
let NERDTreeMouseMode=2
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\.pyc','\~$','\.swo$','\.swp$','\.git','\.hg','\.svn','\.bzr']
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_gui_startup=0
 
set t_Co=256
colorscheme Tomorrow-Night

"" Leader mappings
let mapleader = ","
let maplocalleader = ";"

" Show statusline always
set laststatus=2

set title " change the terminal's title
set noerrorbells " don't beep

" Toggle pastemode easily in insert and command mode
set pastetoggle=<F2>

" Some aliases
command W w
command Q q
command WQ wq
command Wq wq
command Qa qa
command QA qa
command Wa wa
command WA wa

" Run flake8 in every save
" autocmd BufWritePost *.py call Flake8()

" Makes Caps Lock work as Esc
command EscToCapsLock !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'

"" Window management

" new vertical split
command Vertical vertical sp

" new horizontal split
command Horizontal sp

" Move by screen lines instead of file line. Nice with long lines.
nnoremap j gj
nnoremap k gk

" Easily move between split windows using <leader>hjkl
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l

" Easily resize split windows with Ctrl+hjkl
nnoremap <C-j> <C-w>+
nnoremap <C-k> <C-w>-
nnoremap <C-h> <C-w><
nnoremap <C-l> <C-w>>

" fzf 
nnoremap <Leader>bb :Buffers<CR>
nnoremap <Leader>w? :Windows<CR>
nnoremap <Leader>ff :Files<CR>
nnoremap <Leader>gf :GitFiles<CR>


" Force redraw to C-l
nnoremap <Leader>r :redraw!<CR>

" Python
" """"""""
autocmd BufRead,BufNewFile *.py set cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") |
                      \ exe "normal g'\"" | endif   

let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_WinWidth = 50
" map <F4> :TlistToggle<cr>
" map <F8> :!/usr/bin/ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

let g:syntastic_auto_loc_list=1
let g:syntastic_enable_signs=1
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_tex_checkers = ['chktex']


let g:syntastic_mode_map = { 'mode': 'active',
                           \ 'active_filetypes': [],
                           \ 'passive_filetypes': ['python', 'vim','html', 'bib'] }

" Backups
set backupdir=~/.vim/tmp/backup/ " backups
set directory=~/.vim/tmp/swap/ " swap files
set backup " enable backup

set updatetime=1000 
let g:vimchant_spellcheck_lang = 'fi'

" let g:ctrlp_cmd = 'CtrlPLastMode'
" let g:ctrlp_extensions = ['line', 'dir']
" unused exts: tag buffertag
